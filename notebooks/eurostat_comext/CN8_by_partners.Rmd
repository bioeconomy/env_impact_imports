---
title: "Country details"
author: "Paul Rougieux, Marco Follador"
date:  "`r format(Sys.time(), '%d %B, %Y')`"
output:
  word_document: 
    toc: yes
  pdf_document: 
    number_sections: yes
    toc: yes
---

```{r setup, include=FALSE}
# Render this notebook at the command line with
# cd ~/rp/env_impact_imports/notebooks/CN8_main_products/ &&  Rscript -e "rmarkdown::render('CN8_by_partners.Rmd', 'pdf_document')"

# Render this document with less products by reducing the products in the
# vector products_of_interest

knitr::opts_chunk$set(echo = FALSE)
knitr::opts_knit$set(root.dir="../..") # file paths are relative to the root of the project directory
library(dplyr)
# Suppress dplyr warning mesages about groups
options(dplyr.summarise.inform = FALSE)
library(tidyr)
library(ggplot2)
```

```{r}
# Load product aggregation functions
source("R/aggregate_comext.R")
# Select products of interest
# Select products of interest and give them a short name
products_of_interest <- data.frame(productcode2d = c("02", "09", "10", "12", "15",
                                                   "17", "18", "22", "40", "44"),
                                   shortname = c("meat", "coffee", "cereals", "soy", "palm_sunf_oil",
                                                 "sugar", "cocoa", "ethanol", "rubber", "wood"),
                                   stringsAsFactors = FALSE)
```

\newpage

# Introduction

Given a selection of commodities, the purpose of this document is to select the
main partner countries. The analysis is restricted to the following selection
of products which are potentially associated with deforestation or forest
degradation. 


```{r }
products_of_interest %>%
    knitr::kable()
```


## Load EU import data 

Load data from rds files (Serialization Interface for R Objects). 
See the notebook [bio_imports_update_cache.Rmd](bio_imports_update_cache.Rmd) to update the cached data.

```{r echo=FALSE, message=FALSE, warning=FALSE}
# Load metadata
partner_names <- readRDS("data/metadata/partner_names.rds")
reporter_names <- readRDS("data/metadata/reporter_names.rds")
product_names <- readRDS("data/metadata/product_names.rds")

# Load data from cache
system.time(
    yearly <- readRDS("data/comext/extra_eu_bio_imports.rds")
)

yearly <- yearly %>% 
    mutate(year = period %/% 100,
           productcode2d = substr(productcode, 1,2)) %>% 
    right_join(products_of_interest, by = "productcode2d")
```

```{r}
yearly %>%
    group_by(shortname) %>%
    summarise(minyear = min(year),
              maxyear = max(year),
              nflows = n(),
              nreporter = length(unique(reportercode)),
              npartner = length(unique(partnercode)),
              nproducts = length(unique(productcode)),
              value_bE = round(sum(tradevalue, na.rm=TRUE)/1e9),
              weight_bt = round(sum(weight, na.rm=TRUE)/1e9),
              .groups = "drop") %>%
    knitr::kable(caption = "Descriptive statistics of the import flows used in this report. N represents the number of bilateral trade flows. The value is expressed in billion euros over the whole period and the weight in billion tons.")
```


## Keep the main products at 8 digit level

```{r}
product_names_of_interest_8d <- product_names %>%
    mutate(productcode2d = substr(productcode, 1, 2)) %>%
    filter(nchar(productcode) == 8) %>%
    right_join(products_of_interest, by = "productcode2d")
```

There are `r nrow(product_names_of_interest_8d)` product codes at the 8 digit
level for the commodities potentially associated with deforestation and forest
degradation. The following function groups trade flows by the CN2 digit code
and select only the 8 digit product codes which have the largest number of
flows. This is achieved by calculating the percentage of trade within each CN2
product group over the last 5 years and computing a cumulative weight
percentage. Then we keep only the products representing together at least 60%
of the total weight. In the remainder of the document, you will only see the
product codes that passed this selection threshold. 


```{r echo=TRUE}
main_products <- aggregate_products(yearly, product_names) %>% 
    filter(lag_weight_pct_cumul < 0.6) 
```

```{r}
number_of_main_products <- main_products %>%
    group_by(productcode2d) %>% 
    summarise(selected = n())
product_names_of_interest_8d %>%
    group_by(productcode2d, shortname) %>%
    summarise(total_number = n()) %>%
    left_join(number_of_main_products, by = "productcode2d") %>%
    knitr::kable(caption = "Number of 8 digit product codes before and after the selection")
```

*Note*: This selection method may be an issue if some CN8 product codes with a
low level of export to the EU in general turn out to be very important in a
particular country. In that case we could potentially ommit a product
potentially associated with deforestation. It is therefore important to also
look at the overall export flow for each contry, aggregated at the CN2 level.

TODO: take into account changes in product codes. 

```{r eval=FALSE}
# Export list of main products to csv
# This was used to generate a csv file sent to Marco so he could enter the FAO codes by hand inside the file. 
# main_products %>% 
#     ungroup() %>% 
#     select(-productdescription) %>% 
#     left_join(product_names, by=c("productcode")) %>% 
#     select(productcode, productdescription) %>% 
#     distinct() %>% 
#     mutate(productcode_fao = NA) %>% 
#     write.csv("data/hs_to_fao_product_codes.csv", row.names = FALSE)
```

\newpage

# Main EU trade partners for each product over the whole period

For each commodity at the 8 digit level, compute an average of the trade weight
and trade value over the last 10 years. If data is missing for some years, the
average will be computed over the years available for that country. Then sort
the countries by average weight and keep the countries representing together a
cumulative percentage of 90% of total weight exported to the EU.

```{r}
whole_period_by_partners <- yearly %>%
    # Keep only the main products over the last 10 years
    filter(productcode %in% main_products$productcode & 
           year > max(year) - 10) %>%
    # Calculate the sum for each CN8 product code  over the period in each
    # partner country
    group_by(productcode, partnercode, partneriso) %>% 
    summarise(min_year = min(year),
              max_year = max(year),
              nyears = max(year) - min(year) + 1,
              avg_weight = sum(weight, na.rm = TRUE)/nyears,
              avg_tradevalue = sum(tradevalue) / nyears) %>% 
    # Calculate the percentage of trade between partners within each CN8 product group 
    group_by(productcode) %>% 
    arrange(productcode, desc(avg_weight))  %>% 
    mutate(sumavgweight = sum(avg_weight, na.rm = TRUE),
           weight_pct = avg_weight / sumavgweight,
           weight_pct_cumul = cumsum(weight_pct),
           sumavgtradevalue = sum(avg_tradevalue, na.rm = TRUE),
           tradevalue_pct = avg_tradevalue / sumavgtradevalue,
           tradevalue_pct_cumul = cumsum(tradevalue_pct)) %>%
    left_join(product_names, by="productcode") %>%
    left_join(partner_names, by="partnercode") %>% 
    mutate(productdescription = substr(productdescription, 1, 20)) %>%
    # Keep partners representing together 90% or more of the total trade value
    filter(lag(weight_pct_cumul, default = 0) < 0.9)
if(FALSE){
    write.csv(whole_period_by_partners,
              "~/downloads/whole_period_by_partners.csv", row.names=FALSE)
}
```

```{r results='asis'}
# Main partners at 8 digit level
i <- 1
last_short_name <- ""
for (this_product in unique(main_products$productcode)){
    i = i+1
    #if(i==5) break # Short loop for development

    # Use the short name as a top level section title in the web page
    this_short_name <- products_of_interest$shortname[products_of_interest$productcode2d == substr(this_product, 1, 2)]
    # Do not repeat the section title for several consecutive products sharing a 2d code.
    # Only show it when it changes.
    if (this_short_name != last_short_name){
        cat(sprintf('\n\n## %s \n\n', this_short_name))
        last_short_name <- this_short_name
    }

    # Sub section title 
    product_description <- product_names$productdescription[product_names$productcode==this_product]
    cat(sprintf('\n\n%s %s \n\n',this_product, product_description))
    # Filter only products under this CN2 code
    whole_period_by_partners %>%
        filter(productcode == this_product) %>% 
        ungroup() %>% 
        arrange(desc(avg_weight)) %>% 
        # Replace repeated years by empty fields
        mutate(partner = substr(partner, 1, 25)) %>% 
        mutate_at(vars(matches("pct")), ~round(.,digits = 3)*100) %>%
        select(partner, avg_tradevalue, tv_pct = tradevalue_pct, avg_weight, weight_pct) %>% 
        knitr::kable(format="pandoc", format.args = list(big.mark = ",")) %>% 
        print()
}
```

\newpage

# Main EU trade partners for each product in each year

To find out which are the main partners for each product in each year, we first
Keep only a list of 8 digit product codes which represent together at least 60%
of total trade under the given CN2 chapter (group). Then for all trade partners
under each (8 digit) product code, we keep only the partners representing at
least 80% of total weight together.

```{r}
yearly_by_partners <- aggregate_partners(yearly, product_names, partner_names)

main_partners <- yearly_by_partners %>% 
    # Keep only main products
    filter(productcode %in% main_products$productcode) %>% 
    # Keep partners representing together at least 80% of the total weigt
    filter(lag(weight_pct_cumul, default=0) < 0.8) %>% 
    group_by(productcode, partnercode, partneriso) %>% 
    tally()

yearly_by_main_partners <- yearly_by_partners %>% 
    # Filter using a right join
    right_join(main_partners, by=c("productcode", "partnercode", "partneriso"))

```

```{r results='asis'}
# Main partners at 8 digit level
i <- 1
last_short_name <- ""
for (this_product in unique(main_products$productcode)){
    i = i+1
    #if(i==5) break # Short loop for development

    # Use the short name as a top level section title in the web page
    this_short_name <- products_of_interest$shortname[products_of_interest$productcode2d == substr(this_product, 1, 2)]
    # Do not repeat the section title for several consecutive products sharing a 2d code.
    # Only show it when it changes.
    if (this_short_name != last_short_name){
        cat(sprintf('\n\n# %s \n\n', this_short_name))
        last_short_name <- this_short_name
    }

    # Sub section title 
    product_description <- product_names$productdescription[product_names$productcode==this_product]
    cat(sprintf('\n\n## %s %s \n\n',this_product, substr(product_description, 1, 33)))
    cat(product_description, '\n\n')
    # Filter only products under this CN2 code
    yearly_by_main_partners %>% 
        filter(productcode == this_product) %>% 
        ungroup() %>% 
        arrange(year, desc(weight)) %>% 
        # Replace repeated years by empty fields
        mutate(year = ifelse(year==lag(year, default=0),"",year),
               partner = substr(partner, 1, 25)) %>% 
        mutate_at(vars(matches("pct")), ~round(.,digits = 3)*100) %>%
        select(year, partner, tradevalue, tv_pct = tradevalue_pct, weight, weight_pct) %>% 
        knitr::kable(format="pandoc", format.args = list(big.mark = ",")) %>% 
        print()
}
```

# Plot time series

```{r}
yearly_by_main_partners %>%
    filter(productcode == "02013000" & !(partner %in% c("Namibia", "Botswana"))) %>%
    ggplot(aes(x = year, y = tradevalue/1e6, color = partner)) + 
    geom_line() +
    ylab("EU import value in million euros") +
    ggtitle("Fresh or chilled bovine meat, boneless", subtitle = "02013000") #+ 
    #theme(legend.position="bottom")

if(FALSE){
    ggsave("~/downloads/EU_meat_import.pdf", width = 15, height = 10, units = "cm")
}
```




# Export content to csv files

Generate one csv file for each product at the 2 digit level

```{r eval=FALSE}
for(p in products_of_interest$productcode2d){
    yearly %>%
        filter(substr(productcode,1,2) == p)%>%
        head()%>%
        write.csv(file.path(tempdir(),paste0("product",p,".csv")), row.names=FALSE)
    print(p)
}
print(tempdir())
```


```{r eval=FALSE}
# ENcoding issue
# upon compiling this notebook I receive following error:
# ! Package inputenc Error: Unicode character  (U+0092)
# (inputenc)                not set up for use with LaTeX.
# 
# Try other LaTeX engines instead (e.g., xelatex) if you are using pdflatex. For R Markdown users, see https://bookdown.org/yihui/rmarkdown/pdf-document.html
# Error: Failed to compile bio_imports_by_country_CN8.tex. See https://yihui.name/tinytex/r/#debugging for debugging tips. See bio_imports_by_country_CN8.log for more info.
# Execution halted
partner_names$partner[!validUTF8(partner_names$partner)]
product_names$productdescription[!validUTF8(product_names$productdescription)]
# The issue is here according to the last line of the latex log
# ...                                              
#                                                   
# l.3034 & Laos (People
#                        s Democratic Republic) & 28922963 & 0.05 & 
partner_names$partner[grepl("Lao", partner_names$partner)] 


# Other latex error:
# l.8716 COMPOUNDS, IN THE FORM OF A LIQUID AT 20�
#                                                   C, N.E.S. 
# 

product_names$productdescription[grepl("IN THE FORM OF A LIQUID AT",product_names$productdescription)]
#validUTF8(product_names$productdescription[grepl("IN THE FORM OF A LIQUID AT",product_names$productdescription)])
iconv(product_names$productdescription[grepl("IN THE FORM OF A LIQUID AT",product_names$productdescription)], from='utf8')


# Detect file encoding of the CN.txt file
# /tmp/comext/COMEXT_METADATA/CLASSIFICATIONS_AND_RELATIONS/ENGLISH
# Command line utility
# $ uchardet /tmp/comext/COMEXT_METADATA/CLASSIFICATIONS_AND_RELATIONS/ENGLISH/CN.txt 
# UTF-8
# For lack of a better method, 
# I did a replace all for the question mark characters in 
# product_names$productdescription[grepl("�",product_names$productdescription)]
# [1] "WINES PRODUCED IN VALL�E DU RH�NE, IN CONTAINERS HOLDING <= 2 L AND OF AN ACTUAL ALCOHOLIC STRENGTH OF <= 15% VOL, WITH PDO (OTHER THAN SPARKLING WINE, SEMI-SPARKLING WINE AND WHITE WINE)"                                                                        
# [2] "WINES PRODUCED IN VALL�E DU RH�NE, IN CONTAINERS HOLDING > 2 L AND OF AN ACTUAL ALCOHOLIC STRENGTH OF <= 15% VOL, WITH PDO (OTHER THAN SPARKLING WINE, SEMI-SPARKLING WINE AND WHITE WINE)"                                                                         
# [3] "WINES PRODUCED IN EU, IN CONTAINERS HOLDING > 2 L AND OF AN ACTUAL ALCOHOLIC STRENGTH OF <= 15% VOL, WITH PDO (OTHER THAN BORDEAUX, BOURGOGNE, BEAUJOLAIS, VALL�E DU RH�NE, LANGUEDOC-ROUSSILLON, VAL DE LOIRE, SPARKLING WINE, SEMI-SPARKLING WINE AND WHITE WINE)"
# [4] "CHEMICAL PRODUCTS OR PREPARATIONS, PREDOMINANTLY COMPOSED OF ORGANIC COMPOUNDS, IN THE FORM OF A LIQUID AT 20�C, N.E.S."                                                                                                                                            
# [5] "CHEMICAL PRODUCTS OR PREPARATIONS, PREDOMINANTLY COMPOSED OF ORGANIC COMPOUNDS, N.E.S. (EXCL. IN THE FORM OF A LIQUID AT 20�C)"  

```


