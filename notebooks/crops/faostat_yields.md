---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.11.3
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

```python
import pandas
crops = pandas.read_csv('../../data/faostat/crop_production_all.csv',
                  keep_default_na = True, encoding = 'latin-1')
countries = ['Malaysia', 'Indonesia', 'Papua New Guinea']
crop_yield = crops.query('area in @countries & element == "yield"').reset_index(drop = True)
palm_fruit = crops.query("area in @countries & product == 'Oil palm fruit'").reset_index(drop = True)
```

```python
palm_fruit
```

```python
palm_fruit.element.unique()
```

```python

```
