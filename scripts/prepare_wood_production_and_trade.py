#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Written by Paul Rougieux.

JRC Biomass Project.
Unit D1 Bioeconomy.

    Typically you would run this file from a command line like this:

         ipython3 -i -- ~/repos/env_impact_imports/scripts/prepare_wood_production_and_trade.py
"""
# Third party modules
import pandas

# Internal modules
from biotrade.faostat import faostat
from biotrade.faostat.aggregate import agg_trade_eu_row
from biotrade.faostat.convert import convert_to_eq_rwd_level_1

# Display more rows from data frames
pandas.set_option("display.max_rows", 50)

reporter_selected = "Ukraine"
# Load FAOSTAT forestry production data for one country
fp1 = faostat.db_sqlite.select(table="forestry_production", reporter=reporter_selected)
# Load FAOSTAT forestry bilateral trade data for one country
ft1 = faostat.db_sqlite.select(table="forestry_trade", reporter=reporter_selected)
ft1_agg = agg_trade_eu_row(ft1)
index = ["reporter_code", "reporter", "product_group", "element", "unit", "year"]
ft1_agg = (
    ft1_agg.merge(faostat.products.groups, on="product")
    .groupby(index)
    .agg(value=("value", sum))
    .reset_index()
    .rename(columns={"product_group": "product"})
)
ft1_mirror = faostat.db_sqlite.select(
    table="forestry_trade", partner=[reporter_selected]
)
ft1_mirror_agg = agg_trade_eu_row(ft1_mirror, index_side="reporter")

# Product grouping generated from this summary table
# ft1.groupby(['element','product','unit']).agg(value=("value",sum)).to_csv("/tmp/uk.csv")

###########################################
# Convert to roundwood equivalent volumes #
###########################################
ft1eurow_eqr = convert_to_eq_rwd_level_1(ft1_agg)
fp1_eqr = convert_to_eq_rwd_level_1(fp1).drop(
    columns={"element_code", "flag", "period", "product_code"}
)
# check that columns are identical
set(fp1_eqr).symmetric_difference(ft1eurow_eqr)
# keep only commodities in CONVERSION_FACTORS_LEVEL1
# product_selected = CONVERSION_FACTORS_LEVEL1['product'].tolist()
# fp1 = forestry_production.query("reporter == @reporter_selected and product in @product_selected").copy()
# ft1 = forestry_trade.query("reporter == @reporter_selected and product in @product_selected").copy()

# Plot
ft1eurow_eqr["product_partner_group"] = (
    ft1eurow_eqr["product"] + "_" + ft1eurow_eqr["partner_group"]
)
# Trade plot
(
    ft1eurow_eqr.query("element == 'export_quantity'")
    .pivot(index=["year"], columns=["product_partner_group"], values="value_eqrwd")
    .plot()
)

###################################
# Convert to equivalent roundwood #
###################################
fp1eqr = convert_to_eq_rwd_level_1(fp1)

# Production plot
(
    fp1eqr.query("element == 'production'")
    .pivot(index=["year"], columns=["product"], values="value_eqrwd")
    .plot()
)
