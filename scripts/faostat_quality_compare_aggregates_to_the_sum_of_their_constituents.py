#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Written by Paul Rougieux.

JRC Biomass Project.
Unit D1 Bioeconomy.

    Typically you would run this file from a command line like this:

         ipython3 -i -- ~/repos/env_impact_imports/scripts/prepare_wood_production_and_trade.py
"""
# Third party modules
import pandas

# Internal modules
from biotrade.faostat import faostat
from biotrade.faostat.quality import compare_aggregate_to_constituents

# Load FAOSTAT forestry production and trade data
forestry_production = faostat.forestry_production
forestry_trade = faostat.forestry_trade

# Display more rows from data frames
pandas.set_option("display.max_rows", 500)

# Select one country
reporter_selected = "Ukraine"
fp1 = forestry_production.query("reporter == @reporter_selected")

print("Element\n", fp1["element"].unique())

print("Total production over the last 5 years, by product")
(
    fp1.query("year > @fp1.year.max()-5")
    .groupby(["product", "unit"])
    .agg(production=("value", sum))
    .sort_values("production", ascending=False)
)

print("Product names\n", fp1["product"].unique())


def product_contains(df, word):
    """Display product names containing the given word"""
    return df["product"][df["product"].str.contains(word)].unique()


print(product_contains(fp1, "round"))
print(product_contains(fp1, "fuel"))
print(product_contains(fp1, "sawn"))
print(product_contains(fp1, "panel"))
print(product_contains(fp1, "board"))
print(product_contains(fp1, "paper"))
print(product_contains(fp1, "coat"))

#######################################################################################
# Compare production and trade of aggregate products to the sum of their constituents #
#######################################################################################
# Check that roundwood production is the sum of industrial roundwood and fuel wood
# Also check for export and import values
# TODO: place constituents in a table by levels
fp1r = compare_aggregate_to_constituents(
    fp1, "roundwood", ["industrial_roundwood", "wood_fuel"]
)
assert all(fp1r["diff"] == 0)
fp1s = compare_aggregate_to_constituents(
    fp1, "sawnwood", ["sawnwood_coniferous", "sawnwood_non_coniferous_all"]
)
assert all(fp1s["diff"] == 0)

panel_constituents = [
    "plywood",
    "particle_board_and_osb_1961_1994_",
    "particle_board",
    "osb",
    "hardboard",
    "mdf_hdf",
    "other_fibreboard",
    "fibreboard_compressed_1961_1994_",
]
fp1p = compare_aggregate_to_constituents(fp1, "wood_based_panels", panel_constituents)
assert all(fp1p["diff"] == 0)

wood_pulp_constituents = [
    "mechanical_and_semi_chemical_wood_pulp",
    "mechanical_wood_pulp",
    "semi_chemical_wood_pulp",
    "chemical_wood_pulp_sulphate_unbleached",
    "chemical_wood_pulp_sulphate_bleached",
    "chemical_wood_pulp_sulphite",
    "chemical_wood_pulp_sulphite_unbleached",
    "chemical_wood_pulp_sulphite_bleached",
    "dissolving_wood_pulp",
]
fp1wp = compare_aggregate_to_constituents(fp1, "wood_pulp", wood_pulp_constituents)
assert all(fp1wp["diff"] == 0)

# Other aggregates at lower levels
# 'chemical_wood_pulp',
# 'pulp_for_paper',
# 'wood_pulp_excluding_mechanical_wood_pulp',

paper_constituents = [
    "newsprint",
    "printing_and_writing_papers_uncoated_mechanical",
    "printing_and_writing_papers_uncoated_wood_free",
    "printing_and_writing_papers_coated",
    "household_and_sanitary_papers",
    "wrapping_and_packaging_paper_and_paperboard_1961_1997_",
    "case_materials",
    "cartonboard",
    "wrapping_papers",
    "other_papers_mainly_for_packaging",
    "other_paper_and_paperboard_n_e_s_not_elsewhere_specified_",
]
# Level 1 aggregate
fp1pp = compare_aggregate_to_constituents(
    fp1, "paper_and_paperboard", paper_constituents
)
# Constituents were not reported for old years
assert all(fp1pp["diff"] == 0)
# Constituents only exist for recent years
assert all(fp1pp.query("year > 1997")["diff"] == 0)

# Level 2 aggregates
# 'graphic_papers',
# 'other_paper_and_paperboard',
# Level 3 aggregates
# 'printing_and_writing_papers',
# 'packaging_paper_and_paperboard',

# 'paper_and_paperboard_excluding_newsprint',

paper_constituents_level_2 = [
    "graphic_papers",
    "other_paper_and_paperboard",
]
fp1pp2 = compare_aggregate_to_constituents(
    fp1, "paper_and_paperboard", paper_constituents_level_2
)
assert all(fp1pp2["diff"] == 0)

pwp_constituents = [
    "printing_and_writing_papers_uncoated_mechanical",
    "printing_and_writing_papers_uncoated_wood_free",
    "printing_and_writing_papers_coated",
]
fp1pwp = compare_aggregate_to_constituents(
    fp1, "printing_and_writing_papers", pwp_constituents
)
# Constituents were not reported for old years
assert all(fp1pwp["diff"] == 0)
# Constituents only exist for recent years
assert all(fp1pwp.query("year > 1997")["diff"] == 0)
