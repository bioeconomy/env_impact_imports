---
title: "Environmental impact of EU imports"
output: 
  html_document: 
    toc: no
---

<!-- Render this R markdown site with:
Rscript -e "rmarkdown::render_site()"
-->

This site contains trade data focusing on the potential environmental impact of
the import of bio based commodities.


This website was created using [Rmarkdown](http://rmarkdown.rstudio.com/rmarkdown_websites.html) and the [source code](https://gitlab.com/paulrougieux/env_impact_imports) is available.


