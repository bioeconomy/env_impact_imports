let max = null;
const highlightedProducts = ['Cocoa, beans', 'Soybeans', 'Coffee, green', 'Oil palm fruit'];
const format = d3.format(".2f");

const svgTreemapHeader = d3.select("#treemap-header"),
  widthTreemapHeader = +svgTreemapHeader.attr("width"),
  heightTreemapHeader = +svgTreemapHeader.attr("height");

const bar = svgTreemapHeader.append("g");

bar.append("rect")
  .attr("width", widthTreemapHeader)
  .attr("height", heightTreemapHeader)
  .attr("rx", 3)
  .attr("ry", 3)
  .style("fill", "#7a30ba");

bar.append("text")
  .attr("x", "50%")
  .attr("y", "50%")
  .attr("dominant-baseline", "middle")
  .attr("text-anchor", "middle")
  .attr("font-size", "18px")
  .attr("font-weight", "600")
  .attr("fill",  "#ffffff")
  .text("Total: " + data["total_value"].toLocaleString() + " ha");

Array.prototype.max = function() {
  return Math.max.apply(null, this);
};

function findArrayMax() {
  if (max === null) {
    const values = [];
    for (let i = 0; i < data.children.length; i++) {
      values.push(data.children[i].value);
    }
    max = values.max();
  }
  return max;
}

function wrapCellText(d, i, nodes) {
  let self = d3.select(this),
    text = self.text(),
    textLength = this.getComputedTextLength(),
    boundingBox = d3.select(this.parentNode.parentNode).select("rect").node().getBBox(),
    width = boundingBox.width,
    height = boundingBox.height;

  if (i === 0)
    self.attr("font-size", "16px");
  else
    self.attr("font-size", (height + width)/12 + "px");

  if (width < 50 || height < 20 || (height < (20 + parseFloat(getComputedStyle(nodes[i]).fontSize)) && i === 1))
    self.text("");
  else if (textLength > (width - 12)) {
    while (textLength > (width - 12)) {
      text = text.slice(0, -1);
      self.text(text + "...");
      textLength = self.node().getComputedTextLength();
    }
    if (text.lastIndexOf(" ") != -1) {
      text = text.substr(0, text.lastIndexOf(" "));
      self.text(text + "...");
    }
  }
}

const svgTreemap = d3.select("#treemap"),
  widthTreemap = +svgTreemap.attr("width"),
  heightTreemap = +svgTreemap.attr("height");

const treemap = d3.treemap()
  .tile(d3.treemapSquarify)
  .size([widthTreemap, heightTreemap])
  .paddingInner(2);

const root = d3.hierarchy(data)
  .sum(d => d.value)
  .sort((a, b) => b.height - a.height || b.value - a.value);

treemap(root);

const cell = svgTreemap.selectAll("g")
  .data(root.leaves())
  .enter()
  .append("g")
  .attr("transform", d => "translate(" + d.x0 + ", " + d.y0 + ")");

cell.append("rect")
  .attr("id", d => d.data.id)
  .attr("width", d => d.x1 - d.x0)
  .attr("height", d => d.y1 - d.y0)
  .attr("rx", 3)
  .attr("ry", 3)
  .style("fill", (d, i, nodes) => highlightedProducts.includes(d.data.name) ? "#b3003e" : "#2443e0");

cell.append("text")
  .selectAll("tspan")
  .data(d => [d.data.name].concat(format(d.data.value_percentage) + "%"))
  .enter()
  .append("tspan")
  .text(d => d)
  .attr("x", "5px")
  .attr("dy", "1em")
  .attr("font-weight", (d, i, nodes) => i === 0 ? "600" : "400")
  .attr("fill", "#ffffff")
  .each(wrapCellText);

cell.append("title")
  .text(d => "product: " + d.data.name + "\n" + "value: " + d.data.value.toLocaleString() + " ha" + "\n" + "value percentage: " + format(d.data.value_percentage) + "%");
