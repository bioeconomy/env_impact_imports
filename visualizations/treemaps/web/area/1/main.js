const years = ["1961", "1962", "1963", "1964", "1965", "1966", "1967", "1968", "1969", "1970", "1971", "1972", "1973", "1974", "1975", "1976", "1977", "1978", "1979", "1980", "1981", "1982", "1983", "1984", "1985", "1986", "1987", "1988", "1989", "1990", "1991", "1992", "1993", "1994", "1995", "1996", "1997", "1998", "1999", "2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019"];

let options = "";
for (let i = years.length - 1; i >= 0; i--)
   options += "<option value='" + years[i] + "'>" + years[i] + "</option>";
document.getElementById("years").insertAdjacentHTML("afterbegin", options);

function changeYear() {
  max = null;
  const year = document.getElementById("years").value;
  redraw(year);
}

let response, max = null;

const format = d3.format(".2f");

const svgTreemapHeader = d3.select("#treemap-header"),
  widthTreemapHeader = +svgTreemapHeader.attr("width"),
  heightTreemapHeader = +svgTreemapHeader.attr("height");

const bar = svgTreemapHeader.append("g");

bar.append("rect")
  .attr("width", widthTreemapHeader)
  .attr("height", heightTreemapHeader)
  .attr("rx", 3)
  .attr("ry", 3)
  .style("fill", "#fc0341");

bar.append("text")
  .attr("x", "50%")
  .attr("y", "50%")
  .attr("dominant-baseline", "middle")
  .attr("text-anchor", "middle")
  .attr("font-size", "18px")
  .attr("font-weight", "600")
  .attr("fill",  "#ffffff");

const svgTreemap = d3.select("#treemap"),
  widthTreemap = +svgTreemap.attr("width"),
  heightTreemap = +svgTreemap.attr("height");

const treemap = d3.treemap()
  .tile(d3.treemapSquarify)
  .size([widthTreemap, heightTreemap])
  .paddingInner(2);

Array.prototype.max = function() {
  return Math.max.apply(null, this);
};

function findArrayMax() {
  if (max === null) {
    const values = [];
    for (let i = 0; i < data.children.length; i++) {
      values.push(data.children[i].value);
    }
    max = values.max();
  }
  return max;
}

function setOpacity(parentMax, value) {
  const opacity = d3.scaleLinear().domain([0, parentMax]).range([0.2, 1]);
  return opacity(value);
}

function wrapCellText(d, i, nodes) {
  let self = d3.select(this),
    text = self.text(),
    textLength = this.getComputedTextLength(),
    boundingBox = d3.select(this.parentNode.parentNode).select("rect").node().getBBox(),
    width = boundingBox.width,
    height = boundingBox.height;

  if (i === 0)
    self.attr("font-size", "16px");
  else
    self.attr("font-size", (height + width)/12 + "px");

  if (width < 50 || height < 20 || (height < (20 + parseFloat(getComputedStyle(nodes[i]).fontSize)) && i === 1))
    self.text("");
  else if (textLength > (width - 12)) {
    while (textLength > (width - 12)) {
      text = text.slice(0, -1);
      self.text(text + "...");
      textLength = self.node().getComputedTextLength();
    }
    if (text.lastIndexOf(" ") != -1) {
      text = text.substr(0, text.lastIndexOf(" "));
      self.text(text + "...");
    }
  }
}

function redraw (year) {
  data = response[year];

  bar.select("text").text("Total: " + data["total_value"].toLocaleString() + " ha");

  svgTreemap.selectAll("*").remove();

  const root = d3.hierarchy(data)
    .sum(d => d.value)
    .sort((a, b) => b.height - a.height || b.value - a.value);

  treemap(root);

  const cell = svgTreemap.selectAll("g")
    .data(root.leaves())
    .enter()
    .append("g")
    .attr("transform", d => "translate(" + d.x0 + ", " + d.y0 + ")");

  cell.append("rect")
    .attr("id", d => d.data.id)
    .attr("width", d => d.x1 - d.x0)
    .attr("height", d => d.y1 - d.y0)
    .attr("rx", 3)
    .attr("ry", 3)
    .style("fill", d3.color("#4e03fc"))
    .style("opacity", d => setOpacity(findArrayMax(), d.data.value));

  cell.append("text")
    .selectAll("tspan")
    .data(d => [d.data.name].concat(format(d.data.value_percentage) + "%"))
    .enter()
    .append("tspan")
    .text(d => d)
    .attr("x", "5px")
    .attr("dy", "1em")
    .attr("font-weight", (d, i, nodes) => i === 0 ? "600" : "400")
    .attr("fill", "#ffffff")
    .each(wrapCellText);

  cell.append("title")
    .text(d => "product: " + d.data.name + "\n" + "value: " + d.data.value.toLocaleString() + " ha" + "\n" + "value percentage: " + format(d.data.value_percentage) + "%");
}

response = dataBrazil;
redraw(2019);
