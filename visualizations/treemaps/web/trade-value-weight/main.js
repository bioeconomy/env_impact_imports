Array.prototype.max = function() {
  return Math.max.apply(null, this);
};

var groupingParameter = "exporters",
  summingFunction = sumByTradeValue,
  headerTextProperty = "total_trade_value",
  cellTextProperty = "trade_value_percentage",
  unit = "euro";

var data, root, cell,
  format = d3.format(".2f");
  color = d3.scaleOrdinal(["#ff4910", "#e8054f", "#c412ff", "#2305e8", "#1984ff"]);

var svgTreemapHeader = d3.select("#treemap-header"),
  widthTreemapHeader = +svgTreemapHeader.attr("width"),
  heightTreemapHeader = +svgTreemapHeader.attr("height");

var bar = svgTreemapHeader.append("g");

bar.append("rect")
  .attr("width", widthTreemapHeader)
  .attr("height", heightTreemapHeader)
  .attr("rx", 3)
  .attr("ry", 3)
  .style("fill", "#2305e8");

bar.append("text")
  .attr("x", "50%")
  .attr("y", "50%")
  .attr("dominant-baseline", "middle")
  .attr("text-anchor", "middle")
  .attr("font-size", "18px")
  .attr("font-weight", "600")
  .attr("fill",  "#ffffff");

var svgTreemap = d3.select("#treemap"),
  widthTreemap = +svgTreemap.attr("width"),
  heightTreemap = +svgTreemap.attr("height");

var treemap = d3.treemap()
  .tile(d3.treemapSquarify)
  .size([widthTreemap, heightTreemap])
  .paddingTop(16)
  .paddingBottom(10)
  .paddingRight(6)
  .paddingInner(2);

function wrapCellText(d, i, nodes) {
  var self = d3.select(this),
    text = self.text(),
    textLength = this.getComputedTextLength(),
    boundingBox = d3.select(this.parentNode.parentNode).select("rect").node().getBBox(),
    width = boundingBox.width,
    height = boundingBox.height;

  if (i === 0)
    self.attr("font-size", "16px");
  else
    self.attr("font-size", (height + width)/12 + "px");

  if (width < 50 || height < 20 || (height < (20 + parseFloat(getComputedStyle(nodes[i]).fontSize)) && i === 1))
    self.text("");
  else if (textLength > (width - 12)) {
    while (textLength > (width - 12)) {
      text = text.slice(0, -1);
      self.text(text + "...");
      textLength = self.node().getComputedTextLength();
    }
    if (text.lastIndexOf(" ") != -1) {
      text = text.substr(0, text.lastIndexOf(" "));
      self.text(text + "...");
    }
  }
}

function wrapBlockText(d) {
  var self = d3.select(this),
    text = self.text(),
    textLength = this.getComputedTextLength(),
    width = d.x1 - d.x0,
    height = d.y1 - d.y0;

  if (width < 30 || height < 30)
    self.text(".");
  else if (textLength > width) {
    while (textLength > width) {
      text = text.slice(0, -1);
      self.text(text + "...");
      textLength = self.node().getComputedTextLength();
    }
    if (text.lastIndexOf(" ") != -1) {
      text = text.substr(0, text.lastIndexOf(" "));
      self.text(text + "...");
    }
  }
}

function findArrayMax(parentId) {
  for (var i = 0; i < data.children.length; i++) {
    if (data.children[i].id == parentId) {
      var parentArray = [];
      for (var j = 0; j < data.children[i].children.length; j++)
        parentArray.push(data.children[i].children[j][cellTextProperty]);
      return parentArray.max();
    }
  }
}

function setOpacity(parentMax, value) {
  var opacity = d3.scaleLinear().domain([0, parentMax]).range([0.2, 1]);
  return opacity(value);
}

function addTextsTitles() {
  // adds text (2 tspans) to cells
  cell.append("text")
    .selectAll("tspan")
    .data(d => [d.data.name].concat(format(d.data[cellTextProperty]) + "%"))
    .enter()
    .append("tspan")
    .text(d => d)
    .attr("x", "5px")
    .attr("dy", "1em")
    .attr("font-weight", (d, i, nodes) => i === 0 ? "600" : "400")
    .attr("fill", "#ffffff")
    .each(wrapCellText);

  // a simple way to make tooltips
  cell.append("title")
    .text(d => groupingParameter === "exporters" ? "exporting country: " + d.parent.data.name + "\n" + "product: " + d.data.name + "\n" + d.value.toLocaleString() + " " + unit + "\n" + format(d.data[cellTextProperty]) + "%" : "exporting country: " + d.data.name + "\n" + "product: " + d.parent.data.name + "\n" + d.value.toLocaleString() + " " + unit + "\n" + format(d.data[cellTextProperty]) + "%");

  // adds title to blocks
  svgTreemap.selectAll("blocks")
    .data(root.descendants().filter(d => d.depth === 1))
    .enter()
    .append("text")
    .text(d => d.data.name)
    .attr("x", d => d.x0)
    .attr("y", d => d.y0 + 10)
    .attr("font-size", "18px")
    .attr("font-weight", "600")
    .attr("fill", d => color(d.data.id))
    .each(wrapBlockText);
}

function changed(sum) {
  bar.select("text").text("Total: " + data[headerTextProperty].toLocaleString() + " " + unit);

  svgTreemap.selectAll("text").remove();
  svgTreemap.selectAll("title").remove();

  // gives the treemap a new root, which uses a different summing function
  treemap(root.sum(sum));

  // updates the size and position of each rectangle
  cell.attr("transform", d => "translate(" + d.x0 + ", " + d.y0 + ")")
    .select("rect")
    .attr("width", d => d.x1 - d.x0)
    .attr("height", d => d.y1 - d.y0);

  addTextsTitles();
}

function redraw () {
  bar.select("text").text("Total: " + data[headerTextProperty].toLocaleString() + " " + unit);

  svgTreemap.selectAll("*").remove();

  // since we are dealing with hierarchical data, we need to convert the data to the right format
  root = d3.hierarchy(data)
    .eachBefore(d => d.data.id = (d.parent ? d.parent.data.id + "." : "") + d.data.name)
    .sum(summingFunction)
    .sort((a, b) => b.height - a.height || b.value - a.value);

  // computes x0, x1, y0 and y1 for each cell
  treemap(root);

  cell = svgTreemap.selectAll("g")
    .data(root.leaves())
    .enter()
    .append("g")
    .attr("transform", d => "translate(" + d.x0 + ", " + d.y0 + ")");

  // adds rectangles to each of the cells that were generated
  cell.append("rect")
    .attr("id", d => d.data.id)
    .attr("width", d => d.x1 - d.x0)
    .attr("height", d => d.y1 - d.y0)
    .attr("rx", 3)
    .attr("ry", 3)
    // .style("fill", d3.color("steelblue"))
    .style("fill", d => color(d.parent.data.id))
    .style("opacity", d => setOpacity(findArrayMax(d.parent.data.id), d.data[cellTextProperty]));

  addTextsTitles();
}

function groupBy(parameter) {
  if (parameter === "exporters") {
    groupingParameter = "exporters";
    data = dataExporters;
  }
  else if (parameter === "products") {
    groupingParameter = "products";
    data = dataProducts;
  }
  redraw();
}

function sumByTradeValue(d) {
  return d.trade_value;
}

function sumByWeight(d) {
  return d.weight;
}

function sumBy(parameter) {
  if (parameter === "tradeValue") {
    summingFunction = sumByTradeValue;
    headerTextProperty = "total_trade_value";
    cellTextProperty = "trade_value_percentage";
    unit = "euro";
  }
  else if (parameter === "weight") {
    summingFunction = sumByWeight;
    headerTextProperty = "total_weight";
    cellTextProperty = "weight_percentage";
    unit = "kg";
  }
  changed(summingFunction);
}

groupBy(groupingParameter);
document.getElementById("radio-exporters").checked = true;
document.getElementById("radio-trade-value").checked = true;
