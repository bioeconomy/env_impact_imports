import pandas as pd
import json

df = pd.read_csv("../../../data/faostat/crop_production_all.csv")
df = df.query(
    'reporter == "Brazil" & element == "area_harvested" & product_code < 1000'
).reset_index(drop=True)

# group by years and compute the percentage by product in each year
df["total_value"] = df.groupby("year")["value"].transform("sum")
df["value_percentage"] = df["value"] / df["total_value"] * 100

# sort by percentage, compute a cumulative sum and shift it by one
df.sort_values(by=["year", "value_percentage"], ascending=[True, False], inplace=True)
df["cumsum"] = df.groupby("year").value_percentage.cumsum(skipna=True)
df["cumsum_lag"] = df.groupby("year")["cumsum"].transform("shift", fill_value=0)

# sum up harvest areas above the 90 percent threshold under a product called 'Others'
# create a grouping variable called product_2, which will be 'Others' for products above the threshold
df["product_2"] = df["product"].where(df["cumsum_lag"] < 90, "Others")

# group products that are in the 'Others' category and calculate their percentage
index = ["year", "product_2", "total_value"]
df_grouped = df.groupby(index).agg({"value": "sum"}).reset_index()
df_grouped["value_percentage"] = df_grouped["value"] / df_grouped["total_value"] * 100

# with pd.option_context('display.max_rows', None):
# print(df_grouped.head(200))

# print(df_grouped.query('year == 2019'))

# create the JSON file
# note that pandas to_dict() method can be used to generate the JSON file
# see also the various ways to convert pandas DataFrame to nested JSON: https://stackoverflow.com/questions/40470954/convert-pandas-dataframe-to-nested-json
data = {}

for year in df_grouped.year.unique():
    df_year = df_grouped.query("year == @year").reset_index(drop=True)

    data_year = {}
    data_year["name"] = "Brazil"
    data_year["total_value"] = df_year["total_value"][0]
    data_year["children"] = []
    for i in range(len(df_year)):
        data_year_part = {}
        data_year_part["name"] = df_year["product_2"][i]
        data_year_part["value"] = df_year["value"][i]
        data_year_part["value_percentage"] = df_year["value_percentage"][i]
        data_year["children"].append(data_year_part)
    data[str(year)] = data_year

with open("./output/area/2/Brazil.json", "w", encoding="utf-8") as f:
    json.dump(data, f, ensure_ascii=False, indent=4)
