# built-in modules
from pathlib import Path

# from datetime import datetime

# third-party packages
import pandas as pd
import json

# output data directory
output_dir = Path.cwd() / "output/product"

# start_time = datetime.now()

df = pd.read_csv(
    Path.cwd().parent.parent.parent / "data/faostat/crop_production_all.csv"
)

# print('time to read the CSV into a DataFrame', (datetime.now() - start_time).total_seconds() * 1000)

eu27 = [
    "Austria",
    "Belgium",
    "Bulgaria",
    "Croatia",
    "Cyprus",
    "Czechia",
    "Denmark",
    "Estonia",
    "Finland",
    "France",
    "Germany",
    "Greece",
    "Hungary",
    "Ireland",
    "Italy",
    "Latvia",
    "Lithuania",
    "Luxembourg",
    "Malta",
    "Netherlands",
    "Poland",
    "Portugal",
    "Romania",
    "Slovakia",
    "Slovenia",
    "Spain",
    "Sweden",
]

# check that all the EU27 reporter names are present in the data
assert set(eu27) - set(df.reporter.unique()) == set()


def select_reporters_with_largest_element(df, years, product, element, threshold=90):
    """Selects reporters with the largest element (production (in tonnes) or harvested area (in ha))
    for the given product based on the threshold."""

    # start_time = datetime.now()

    # remove China to avoid double counting, remove codes above 5000 which are aggregates
    df = df.query(
        "reporter_code < 5000 & reporter_code != 351 & year in @years & product == @product & element == @element"
    ).reset_index(drop=True)

    # label reporters that belong to the same group defined via a list (eu27) in a new column named reporter_grouped
    df["reporter_grouped"] = df["reporter"].where(~df["reporter"].isin(eu27), "EU27")

    # sum the values for the reporters in the same group in the given years
    df = (
        df.groupby("reporter_grouped").agg({"value": "sum"}).reset_index()
    )  # skipna is True for sum by default

    df["total_value"] = df.value.sum()
    df["value_percentage"] = df["value"] / df["total_value"] * 100

    # sort by percentage, compute the cumulative sum and shift it by one
    df.sort_values(by="value_percentage", ascending=False, inplace=True)
    df["cumsum"] = df.value_percentage.cumsum()  # skipna is True for cumsum by default
    df["cumsum_lag"] = df["cumsum"].transform("shift", fill_value=0)

    # label elements above the threshold under a reporter called 'Others'
    # create a grouping variable called reporter_grouped_2, which will be 'Others' for reporters above the threshold
    df["reporter_grouped_2"] = df["reporter_grouped"].where(
        df["cumsum_lag"] < threshold, "Others"
    )

    # group reporters that are in the 'Others' category and calculate their percentage
    df = df.groupby("reporter_grouped_2").agg({"value": "sum"}).reset_index()
    df["total_value"] = df.value.sum()
    df["value_percentage"] = df["value"] / df["total_value"] * 100

    # print('time for processing:', (datetime.now() - start_time).total_seconds() * 1000)

    return df


def create_csv_json(df, years, product, element, threshold=90, output_dir=output_dir):
    """Create the JSON file for treemap visualization with D3.js.

    :param DataFrame df: crop production data of FAOSTAT
    :param list years: the years to be selected
    :param str product: the product to be selected
    :param str element: the element to be selected, it is expected to be either production or area_harvested
    :param float threshold: selected reporters represent the element below this threshold, it can be greater than 0 and less than or equal to 100"""

    df = select_reporters_with_largest_element(df, years, product, element, threshold)

    # start_time = datetime.now()

    file_name = (
        product.lower().replace(",", "").replace(" ", "_")
        + "_"
        + element
        + "_"
        + str(years[0])
    )
    if len(years) > 1:
        file_name += "_" + str(years[-1])

    df.to_csv(output_dir / (file_name + ".csv"), index=False)

    data = {}
    data["name"] = product
    data["total_value"] = df["total_value"][0]
    data["children"] = []
    for i in range(len(df)):
        data_part = {}
        data_part["name"] = df["reporter_grouped_2"][i]
        data_part["value"] = df["value"][i]
        data_part["value_percentage"] = df["value_percentage"][i]
        data["children"].append(data_part)

    with open(output_dir / (file_name + ".json"), "w", encoding="utf-8") as f:
        json.dump(data, f, ensure_ascii=False, indent=4)

    # print('time for creating JSON:', (datetime.now() - start_time).total_seconds() * 1000)


# create_csv_json(df, [2004, 2005, 2006, 2007, 2008], "Soybeans", "area_harvested")
# create_csv_json(df, [2009, 2010, 2011, 2012, 2013], "Soybeans", "area_harvested")
# create_csv_json(df, [2014, 2015, 2016, 2017, 2018], "Soybeans", "area_harvested")
# create_csv_json(df, [2019], "Soybeans", "area_harvested")
#
# create_csv_json(df, [2004, 2005, 2006, 2007, 2008], "Oil palm fruit", "area_harvested")
# create_csv_json(df, [2009, 2010, 2011, 2012, 2013], "Oil palm fruit", "area_harvested")
# create_csv_json(df, [2014, 2015, 2016, 2017, 2018], "Oil palm fruit", "area_harvested")
# create_csv_json(df, [2019], "Oil palm fruit", "area_harvested")
#
# create_csv_json(df, [2004, 2005, 2006, 2007, 2008], "Coffee, green", "area_harvested")
# create_csv_json(df, [2009, 2010, 2011, 2012, 2013], "Coffee, green", "area_harvested")
# create_csv_json(df, [2014, 2015, 2016, 2017, 2018], "Coffee, green", "area_harvested")
# create_csv_json(df, [2019], "Coffee, green", "area_harvested")
#
# create_csv_json(df, [2004, 2005, 2006, 2007, 2008], "Cocoa, beans", "area_harvested")
# create_csv_json(df, [2009, 2010, 2011, 2012, 2013], "Cocoa, beans", "area_harvested")
# create_csv_json(df, [2014, 2015, 2016, 2017, 2018], "Cocoa, beans", "area_harvested")
# create_csv_json(df, [2019], "Cocoa, beans", "area_harvested")
#
# create_csv_json(df, [2004, 2005, 2006, 2007, 2008], "Soybeans", "production")
# create_csv_json(df, [2009, 2010, 2011, 2012, 2013], "Soybeans", "production")
# create_csv_json(df, [2014, 2015, 2016, 2017, 2018], "Soybeans", "production")
# create_csv_json(df, [2019], "Soybeans", "production")
#
# create_csv_json(df, [2004, 2005, 2006, 2007, 2008], "Oil palm fruit", "production")
# create_csv_json(df, [2009, 2010, 2011, 2012, 2013], "Oil palm fruit", "production")
create_csv_json(df, [2014, 2015, 2016, 2017, 2018], "Oil palm fruit", "production")
# create_csv_json(df, [2019], "Oil palm fruit", "production")
#
# create_csv_json(df, [2004, 2005, 2006, 2007, 2008], "Coffee, green", "production")
# create_csv_json(df, [2009, 2010, 2011, 2012, 2013], "Coffee, green", "production")
# create_csv_json(df, [2014, 2015, 2016, 2017, 2018], "Coffee, green", "production")
# create_csv_json(df, [2019], "Coffee, green", "production")
#
# create_csv_json(df, [2004, 2005, 2006, 2007, 2008], "Cocoa, beans", "production")
# create_csv_json(df, [2009, 2010, 2011, 2012, 2013], "Cocoa, beans", "production")
# create_csv_json(df, [2014, 2015, 2016, 2017, 2018], "Cocoa, beans", "production")
# create_csv_json(df, [2019], "Cocoa, beans", "production")
#
# create_csv_json(
#     df, [2004, 2005, 2006, 2007, 2008], "Beef and Buffalo Meat", "production"
# )
# create_csv_json(
#     df, [2009, 2010, 2011, 2012, 2013], "Beef and Buffalo Meat", "production"
# )
# create_csv_json(
#     df, [2014, 2015, 2016, 2017, 2018], "Beef and Buffalo Meat", "production"
# )
# create_csv_json(df, [2019], "Beef and Buffalo Meat", "production")
